let api = require('./answerQuestionAPI.js');
let express = require('express');
let app = express();


app.get('/getAnswers', function (req, res) {
    let text = decodeURIComponent(req.query.question);
    let result = {};
    api.searchForItem(text).then(output, console.error);

    function output(result) {
        console.log(result);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(result));
    }
});

let server = app.listen(8088, function () {

    let host = server.address().address
    let port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)

})