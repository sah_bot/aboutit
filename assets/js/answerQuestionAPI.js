let google = require('google');
let httpClient = require('superagent');
let wordStrip = require('stopword');
let crawler = require('node-read');


google.resultsPerPage = 10;

let googleResults = [];
let wordsToSearch = [];
let resultArray = [];

// searchForItem('What are the dimensions of Panasonic NA-148XS1WGN');
let searchForItem = async function (query) {
    //we get the top 10 results
    return new Promise(function (resolve, reject) {
        google(query, function (err, res) {
            if (err) console.log(err);

            googleResults = res.links;
            //construct some associate words
            findAssociateWords(wordStrip.removeStopwords(query.split(' ')))
                .then(function () {
                    //finished filling up the wordsToSearch

                    let promises = [];
                    //try to find results
                    googleResults.forEach(function (linkObj) {
                        //go link by link and try to find some result
                        promises.push(parsePage(query, linkObj));
                    });

                    Promise.all(promises).then(() => {
                        resolve(resultArray);
                    }).catch((e) => {
                        //TODO:
                    });
                    //TODO: I hate JS :)) (Java dev)
                });
        });
    });
};

let findAssociateWords = async function (query) {
    let awaitResponse = await httpClient
        .get('https://api.wordassociations.net/associations/v1.0/json/search?apikey=ef7c667d-d954-4691-b28a-19fe083718c1')
        .query({lang: 'en', text: query, type: 'response', pos: 'noun', limit: 20});

    //list of words associated to original query
    awaitResponse.body.response.forEach(function (respObj) {
        if (respObj.items.length > 0) {
            wordsToSearch.push(respObj.text);
            wordsToSearch.push.apply(wordsToSearch, respObj.items.map(function (obj) {
                return obj.item;
            }));
        }
    });

    return wordsToSearch;
};

let parsePage = async function (initialQuery, linkObj) {
    if (!linkObj) return '';

    let pageUrl = linkObj.href;
    //crawl the page
    // console.log('To parse this page:' + linkObj.href + '\n' + linkObj.description);

    return new Promise(function (resolve, reject) {
        crawler(linkObj.href, function (err, article, res) {

            if (err || !article.html) return reject('');

            // Main Article.
            let content = article.html;

            let jsdom = require("jsdom");
            let {JSDOM} = jsdom;
            let {window} = new JSDOM(content);
            let $ = require('jquery')(window);

            let foundWordAnswerValuesCount = 0;

            let errorResultMessage = "Could not find answer inside " + '\n'
                + 'Page title: ' + linkObj.title + '\n'
                + 'Page link: ' + linkObj.href + '\n';

            let successResultMessage = 'Inside ' + '\n'
                + 'Page title: ' + linkObj.title + '\n'
                + 'Page link: ' + linkObj.href + '\n'
                + 'we found: \n';

            //search keywords inside
            //TODO: make general implementation
            //For the washing machines we found that most websites used tables to show specs
            //First find a keyword inside a <td>[keyword]</td> and take the result from the following <td>[result]</td>

            wordsToSearch.forEach(word => {
                let foundValue = $('td:contains(' + word + ')').next().text(function () {
                    return $(this).text();
                }).text();
                if (foundValue.length > 0) {
                    successResultMessage += word + ':' + foundValue + '\n';
                    foundWordAnswerValuesCount++;
                }
            });

            let displayMessage;
            if (foundWordAnswerValuesCount == 0) {
                displayMessage = errorResultMessage;
            } else {
                displayMessage = successResultMessage;
            }

            resultArray.push(displayMessage);
            return resolve(displayMessage);
        });
    });
};

module.exports = {
    searchForItem: searchForItem,
    findAssociateWords: findAssociateWords,
    parsePage: parsePage
};